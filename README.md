# sa-qotd

## Background

This project contains a service that operates over the "quote of the
day" service described in [RFC
865](https://www.rfc-editor.org/rfc/rfc865).  We (ab)use that protocol
to provide basic host info to queries on the network.

This service is written to leverage systemd, and so is only made to
run on Linux hosts with systemd.  Right now, the service provides the
hostname of the machine it is running on, along with the IP addresses
configured on the machine.

The need for the services arises from our use of anycast addresses for
several of our machines, and it can be helpful to have a way to know
which machine is answering for an IP address at any time.  Without a
direct-query service like this, you would need to inspect the routing
tables in the core network to know where the anycast packets are being
routed.

You could SSH to the hosts and run commands (such as `hostname`), but
anycast makes this more complicated.  The SSH keys change between
hosts, resulting in warnings.  Additionally, if per-packet load
balancing is being used with anycast, TCP connections will not set up
correctly as they are distributed between machines.  This service runs
on TCP and UDP, allowing it to work.

The daemon does not use any information sent by the client; as soon as
a TCP connection is established or a UDP datagram is received, the
daemon responds with the host information.  TCP connections are
immediately terminated and no further processing is performed.

Because of the risk for amplification attacks (UDP source spoofing
where reply data is much larger than the request), we restrict the
source addresses that may query this service.  You may need to edit
`systemd/sa-qotd.socket` if you do not like the defaults (specific to
Suffield Academy).


## Installation

This distribution includes an installer script to help with
configuring systemd and setting machine-specific options.

First, clone this repository and move into the project folder:

```
git clone https://gitlab.com/suffieldacademy/sa-qotd.git
cd sa-qotd
```

Then, run the install script.  You must pass any IPv4/IPv6 addresses
that you wish to listen on as parameters.  These addresses should be
your anycast addresses on the machine.  The reason for specific
addresses is that UDP replies may not be sourced from the correct IP
if you only listen on a wildcard address (such as [::]).

```
./bin/install <ip address> ...
```

The script will link the systemd unit files into the main system,
configure the listen addresses, add a pass rule to the `ufw` firewall
(if active), and start the service.

At that point, you should be up and running!


## Querying

To query the service, you can use any tool that is capable of
generating a TCP or UDP connection to the service.  Netcat is probably
the simplest, here are some examples:

For TCP, you merely need to initiate a connection to the qotd port
(17):

```
nc hostname.suffieldacademy.org 17
```

UDP is connectionless, so you need to generate a packet to the port in
order to get a response.  The "-w 2" for netcat quits the program
after 2 seconds (otherwise it will remain open indefinitely waiting
for more input):

```
echo "" | nc -u -w 2 hostname.suffieldacademy.org 17
```

You can put anything in the request message; it is ignored.

Data are cached to make the service as low-impact as possible
(hostnames and IPs don't change much).  If you need data to refresh
more quickly, edit the qotd script and change the `$CACHE_TIME`
variable.
